(function(){
    var URL = "ws://"+document.domain+":5000/websocket";
    $(document).ready(function(){
        if ("WebSocket" in window) {
            /* initialize the websocket
             * key method is ws.send(msg)
             * listener on ws.onmessage
             */
            ws = new WebSocket(URL);
            ws.onmessage = function (msg) {

                var n = $("<div>"+msg.data+"</div>");
                setTimeout(function(){
                    n.fadeOut(1000, function(){
                        n.remove();
                    });
                }, 10000);

                $("p#log").prepend(n);
            };

            // Cleanly close websocket when unload window
            window.onbeforeunload = function() {
                ws.onclose = function () {}; // disable onclose handler first
                ws.close()
            };

        }else{
            $("html").html("Your browser does not support websockets, sorry :(");
        }
        // Bind send button to websocket
        $("button#stop").on("click", function() {
            ws.send("__stop__");
        });
        $("button#start").on("click", function() {
            ws.send("__start__");
        });
        $("form#filter").submit(function(){
            $track = $("#track");

            ws.send($track.val());
            $track.val('');
            return false;
        });
    });
})();