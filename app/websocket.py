from actors import TwitterStreamer, UserController
import log
import os
import gevent
logger = log.getLogger(__name__)

APP_KEY = os.environ['APP_KEY']
APP_SECRET = os.environ['APP_SECRET']
AB_OAUTH_TOKEN = os.environ['AB_OAUTH_TOKEN']
AB_OAUTH_TOKEN_SECRET = os.environ['AB_OAUTH_TOKEN_SECRET']

def handle_websocket(ws):
    logger.info('starting websocket')

    streamer = TwitterStreamer(APP_KEY, APP_SECRET, AB_OAUTH_TOKEN, AB_OAUTH_TOKEN_SECRET, websocket=ws)
    user = UserController(ws, streamer)

    user.start()
    streamer.start()
    greenlets = [user, streamer]
    gevent.joinall(greenlets)

