from twython import __version__
from twython.compat import json, is_py3

import requests
from requests_oauthlib import OAuth1

import time
import gevent
from gevent.queue import Queue
from Queue import Empty
import log

logger = log.getLogger(__name__)

class Actor(gevent.Greenlet):

    def __init__(self, *args, **kwargs):
        self.inbox = Queue()
        super(Actor,self).__init__()

    def receive(self, message):
        raise NotImplemented()

    def process_messages(self):
        message = self.inbox.get()
        self.receive(message)
    def _run(self):
        """crude implementation, override in subclasses"""
        self.running = True
        while self.running:
            self.process_messages()

class UserController(Actor):
    def __init__(self, websocket,  twitter_streamer):
        super(UserController, self).__init__()
        self.logger = log.getClassLogger(self, __name__)
        self.streamer = twitter_streamer
        self.websocket = websocket
    def msg_streamer(self, msg):
        self.streamer.inbox.put(msg)
    def _run(self):
        self.running = True
        ws = self.websocket
        self.logger.info('running UserController')

        while self.running:
            message = ws.receive()
            if message is None:
                #when the client closes the connection
                self.msg_streamer('__close__')
                self.running = False
            else:
                #r  = "Got message : '%s', processing..." % message
                #ws.send(r)
                self.logger.info('sending user message %s to the streamer', message)
                self.msg_streamer(message)

            gevent.sleep(0)


class TwitterStreamer(Actor):
    """class heavily based on TwythonStreamer"""

    def __init__(self, app_key, app_secret, oauth_token, oauth_token_secret,
                 timeout=3, retry_count=None, retry_in=10, headers=None, websocket=None):
        """Streaming class for a friendly streaming user experience
        Authentication IS required to use the Twitter Streaming API

        :param app_key: (required) Your applications key
        :param app_secret: (required) Your applications secret key
        :param oauth_token: (required) Used with oauth_token_secret to make
                            authenticated calls
        :param oauth_token_secret: (required) Used with oauth_token to make
                                   authenticated calls
        :param timeout: (optional) How long (in secs) the streamer should wait
                        for a response from Twitter Streaming API
        :param retry_count: (optional) Number of times the API call should be
                            retired
        :param retry_in: (optional) Amount of time (in secs) the previous
                         API call should be tried again
        :param headers: (optional) Custom headers to send along with the
                        request
        """
        super(TwitterStreamer, self).__init__()
        self.websocket = websocket
        self.logger = log.getClassLogger(self, __name__)

        self.auth = OAuth1(app_key, app_secret,
                           oauth_token, oauth_token_secret)

        self.headers = {'User-Agent': 'Twython Streaming v' + __version__}
        if headers:
            self.headers.update(headers)

        self.client = requests.Session()
        self.client.auth = self.auth
        self.client.headers = self.headers
        self.client.stream = True

        self.timeout = timeout

        self.api_version = '1.1'

        self.retry_in = retry_in
        self.retry_count = retry_count

        # Set up type methods
        self.statuses = {
                "filter" : 'https://stream.twitter.com/%s/statuses/filter.json'  % self.api_version,
                'sample' : 'https://stream.twitter.com/%s/statuses/sample.json' % self.api_version,
                'firehost' : 'https://stream.twitter.com/%s/statuses/firehose.json' % self.api_version
        }
        self.user = 'https://userstream.twitter.com/%s/user.json' % self.api_version
        self.site = 'https://sitestream.twitter.com/%s/site.json' % self.api_version

        self.connected = False

        self.url = self.statuses['filter']
        self.params = {'track' : 'fuck you'}
        self.should_stream = False
    def _run(self):
        self.running = True
        self.should_stream = True
        while self.running:
            if self.should_stream:
                try:
                    self.logger.info('streaming from url %s with params %s', self.url, str(self.params))
                    self.stream(self.url, params=self.params)
                except requests.ConnectionError, e:
                    self.logger.error('Connection error: %s', str(e))
                except Exception, e:
                    self.logger.error('%s', str(e))
            self.process_messages()
            gevent.sleep(0)

    def process_messages(self):
        try:
            message = self.inbox.get(block=False)
            self.receive(message)
        except Empty:
            # if there is nothing on the message queue ignore it
            pass
    def stop_streaming(self):
        #stop streaming in the current call
        self.disconnect()
        #don't start streaming again
        self.should_stream = False

    def receive(self, message):
        if message:
            self.logger.info("GOT MESSAGE %s", message)
            if message == "__stop__":
                self.stop_streaming()
            elif message == '__start__':
                self.should_stream = True
            elif message == "__close__":
                self.logger.info("closing the streamer...")
                self.stop_streaming()
                self.running = False
            else:
                self.stop_streaming()
                gevent.sleep(1)
                self.params = {'track' : message}
                self.should_stream = True #start up again

    def stream(self, url, method='GET', params=None):
        """Internal stream request handling"""
        self.connected = True
        retry_counter = 0

        method = method.lower()
        func = getattr(self.client, method)

        def _send(retry_counter):
            while self.connected:
                self.logger.info('  sending request...')
                try:
                    if method == 'get':
                        response = func(url, params=params, timeout=self.timeout)
                    else:
                        response = func(url, data=params, timeout=self.timeout)
                except requests.exceptions.Timeout:
                    self.on_timeout()
                else:
                    if response.status_code != 200:
                        self.on_error(response.status_code, response.content)

                    if self.retry_count and (self.retry_count - retry_counter) > 0:
                        time.sleep(self.retry_in)
                        retry_counter += 1
                        _send(retry_counter)
                    self.logger.info("done sending request")
                    return response

        while self.connected:
            self.logger.info('  streaming...')
            gevent.sleep(0)
            response = _send(retry_counter)
            self.logger.info('got response: %s', str(response))
            lines = response.iter_lines()
            for line in lines:
                self.logger.info('  %s', line)
                gevent.sleep(.5)
                if not self.connected:
                    break
                if line:
                    self.process_messages()
                    try:
                        if not is_py3:
                            self.on_success(json.loads(line))
                        else:
                            line = line.decode('utf-8')
                            self.on_success(json.loads(line))
                    except ValueError:
                        self.on_error(response.status_code, 'Unable to decode response, not vaild JSON.')
        response.close()

    def on_success(self, data):  # pragma: no cover
        """Called when data has been successfull received from the stream
        :param data: data recieved from the stream
        :type data: dict
        """
        if 'text' in data and 'created_at' in data:
            t =  data['text'].encode('utf-8')
            #d = parser.parse(data['created_at'])
            self.websocket.send(t)


    def on_error(self, status_code, data):  # pragma: no cover
        """Called when stream returns non-200 status code
        :param status_code: Non-200 status code sent from stream
        :type status_code: int

        :param data: Error message sent from stream
        :type data: dict
        """
        return

    def on_delete(self, data):  # pragma: no cover
        """Called when a deletion notice is received
        Twitter docs for deletion notices: http://spen.se/8qujd
        :param data: data from the 'delete' key recieved from the stream
        :type data: dict
        """
        return

    def on_limit(self, data):  # pragma: no cover
        """Called when a limit notice is received
        Twitter docs for limit notices: http://spen.se/hzt0b

        :param data: data from the 'limit' key recieved from the stream
        :type data: dict
        """
        return

    def on_disconnect(self, data):  # pragma: no cover
        """Called when a disconnect notice is received

        Twitter docs for disconnect notices: http://spen.se/xb6mm

        :param data: data from the 'disconnect' key recieved from the stream
        :type data: dict
        """
        return

    def on_timeout(self):  # pragma: no cover
        """ Called when the request has timed out """
        return

    def disconnect(self):
        """Used to disconnect the streaming client manually"""
        self.connected = False
