import os

from flask import Flask
from websocket import handle_websocket
import log

logger = log.getLogger(__name__)

app = Flask(__name__)
app.secret_key = os.urandom(24)
app.debug = True


def tweeter_app(environ, start_response):
    path = environ["PATH_INFO"]
    logger.info('serving %s', path)
    if path == "/":
        return app(environ, start_response)
    elif path == "/websocket":
        handle_websocket(environ["wsgi.websocket"])
    else:
        return app(environ, start_response)

import views
