#!/usr/bin/env python
# coding: utf-8
from gevent.pywsgi import WSGIServer
from geventwebsocket.handler import WebSocketHandler

from app import tweeter_app
import os
PORT = os.environ['PORT']

if __name__ == '__main__':
    http_server = WSGIServer(('',int(PORT)), tweeter_app, handler_class=WebSocketHandler)
    http_server.serve_forever()
